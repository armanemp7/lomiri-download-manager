set(TARGET lomiri-upload-manager-priv)

set(SOURCES
        lomiri/uploads/daemon.cpp
        lomiri/uploads/factory.cpp
	lomiri/uploads/manager.cpp
        lomiri/uploads/file_upload.cpp
        lomiri/uploads/manager.cpp
        lomiri/uploads/mms_file_upload.cpp
        lomiri/uploads/upload_adaptor.cpp
        lomiri/uploads/upload_adaptor_factory.cpp
        lomiri/uploads/upload_manager_adaptor.cpp
        lomiri/uploads/upload_manager_factory.cpp
)

set(HEADERS
        lomiri/uploads/daemon.h
        lomiri/uploads/factory.h
	lomiri/uploads/manager.h
        lomiri/uploads/file_upload.h
        lomiri/uploads/manager.h
        lomiri/uploads/mms_file_upload.h
        lomiri/uploads/upload_adaptor.h
        lomiri/uploads/upload_adaptor_factory.h
        lomiri/uploads/upload_manager_adaptor.h
        lomiri/uploads/upload_manager_factory.h
)

include_directories(${Qt5DBus_INCLUDE_DIRS})
include_directories(${Qt5Network_INCLUDE_DIRS})
include_directories(${Qt5Sql_INCLUDE_DIRS})
include_directories(${DBUS_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_SOURCE_DIR}/src/common/public)
include_directories(${CMAKE_SOURCE_DIR}/src/common/priv)
include_directories(${CMAKE_SOURCE_DIR}/src/uploads/common)

add_library(${TARGET} STATIC
	${HEADERS}
	${SOURCES}
)

set_target_properties(
	${TARGET}

	PROPERTIES
	VERSION ${LDM_VERSION_MAJOR}.${LDM_VERSION_MINOR}.${LDM_VERSION_PATCH}
	SOVERSION ${LDM_VERSION_MAJOR}
)

link_directories(${NIH_DBUS_LIBDIR})
link_directories(${GLOG_DBUS_LIBDIR})

target_link_libraries(${TARGET}
	${NIH_DBUS_LIBRARIES}
	${GLOG_LIBRARIES}
	${Qt5DBus_LIBRARIES}
	${Qt5Sql_LIBRARIES}
	ldm-common
	ldm-priv-common
	lomiri-upload-manager-common
)

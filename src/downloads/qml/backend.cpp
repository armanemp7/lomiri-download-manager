#include <QtQml>
#include <QtQml/QQmlContext>
#include "backend.h"
#include "download_error.h"
#include "metadata.h"
#include "single_download.h"
#include "lomiri_download_manager.h"


void BackendPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("Lomiri.DownloadManager"));

    qmlRegisterType<Lomiri::DownloadManager::DownloadError>(uri, 0, 1, "Error");
    qmlRegisterType<Lomiri::DownloadManager::Metadata>(uri, 0, 1, "Metadata");
    qmlRegisterType<Lomiri::DownloadManager::SingleDownload>(uri, 0, 1, "SingleDownload");
    qmlRegisterType<Lomiri::DownloadManager::LomiriDownloadManager>(uri, 0, 1, "DownloadManager");
    qmlRegisterType<Lomiri::DownloadManager::DownloadError>(uri, 1, 2, "Error");
    qmlRegisterType<Lomiri::DownloadManager::Metadata>(uri, 1, 2, "Metadata");
    qmlRegisterType<Lomiri::DownloadManager::SingleDownload>(uri, 1, 2, "SingleDownload");
    qmlRegisterType<Lomiri::DownloadManager::LomiriDownloadManager>(uri, 1, 2, "DownloadManager");
}

void BackendPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    QQmlExtensionPlugin::initializeEngine(engine, uri);
}

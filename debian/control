Source: lomiri-download-manager
Section: net
Priority: optional
Build-Depends: cmake,
               cmake-extras,
               debhelper (>= 9),
               dh-apparmor,
               dh-translations,
               doxygen,
               dbus-test-runner,
               google-mock,
               qtbase5-dev,
               libboost-log-dev,
               libboost-program-options-dev,
               libdbus-1-dev,
               libqt5sql5-sqlite,
               libnih-dbus-dev,
               libgoogle-glog-dev,
               python3,
               qtdeclarative5-dev,
               qtdeclarative5-dev-tools,
               qtdeclarative5-test-plugin,
               qttools5-dev-tools,
               network-manager,
Maintainer: UBports Team <developers@ubports.com>
Standards-Version: 3.9.5
Homepage: https://gitlab.com/ubports/core/lomiri-download-manager/

Package: libldm-common0
Section: libs
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Upload Download Manager - shared common library
 Upload Download Manager performs uploads and downloads from a centralized
 location.
 .
 This package includes the common shared library between the client lib and the
 service lib.

Package: libldm-common-dev
Section: libdevel
Architecture: any
Depends: libldm-common0 (= ${binary:Version}),
         qtbase5-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Description: QT library for Lomiri Download Manager - development files
 This package contains the common headers shared between the client library and
 the daemon library.

Package: libldm-priv-common0
Section: libs
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libldm-common0 (= ${binary:Version}),
Description: Upload Download Manager - shared private library
 Upload Download Manager performs downloads from a centralized location.
 .
 This package includes the private shared library.

Package: liblomiri-download-manager-common0
Section: libs
Architecture: any
Depends: libldm-common0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri Download Manager - shared common library
 Lomiri Download Manager performs downloads from a centralized location.
 .
 This package includes the common shared library between the client lib and the
 service lib.

Package: liblomiri-download-manager-common-dev
Section: libdevel
Architecture: any
Depends: libldm-common-dev (= ${binary:Version}),
         liblomiri-download-manager-common0 (= ${binary:Version}),
         qtbase5-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Description: QT library for Lomiri Download Manager - development files
 This package contains the common headers shared between the client library and
 the daemon library.

Package: liblomiri-download-manager-client0
Section: libs
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libldm-common0 (= ${binary:Version}),
         liblomiri-download-manager-common0 (= ${binary:Version}),
Description: Lomiri Download Manager - shared public library
 Lomiri Download Manager performs downloads from a centralized location.
 .
 This package includes the public shared library.

Package: liblomiri-download-manager-client-dev
Section: libdevel
Architecture: any
Depends: libldm-common-dev (= ${binary:Version}),
         liblomiri-download-manager-common-dev (= ${binary:Version}),
         liblomiri-download-manager-client0 (= ${binary:Version}),
         qtbase5-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Breaks: lomiri-download-manager-client-dev
Replaces: lomiri-download-manager-client-dev
Description: QT library for Lomiri Download Manager - development files
 This package contains the header that can be used to allow an application use
 the Lomiri Download Manager client library.

Package: liblomiri-download-manager-client-doc
Section: doc
Architecture: all
Depends: liblomiri-download-manager-client-dev (>= ${source:Version}),
          ${misc:Depends},
Description: Documentation files for libcontent-hub-dev
 Documentation files for the libcontent-hub development

Package: liblomiri-upload-manager-common0
Section: libs
Architecture: any
Depends: libldm-common0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri Upload Manager - shared common library
 Lomiri Upload Manager performs uploads from a centralized location.
 .
 This package includes the common shared library between the client lib and the
 service lib.

Package: liblomiri-upload-manager-common-dev
Section: libdevel
Architecture: any
Depends: libldm-common-dev (= ${binary:Version}),
         liblomiri-download-manager-common0 (= ${binary:Version}),
         qtbase5-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Description: QT library for Lomiri Upload Manager - development files
 This package contains the common headers shared between the client library and
 the daemon library.

Package: lomiri-download-manager
Section: net
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libldm-common0 (= ${binary:Version}),
         libldm-priv-common0 (= ${binary:Version}),
         liblomiri-download-manager-common0 (= ${binary:Version}),
         unzip
Suggests: apparmor
Description: Lomiri Download Manager - daemon
 Lomiri Download Manager performs downloads from a centralized location.
 .
 This package includes the daemon.

Package: lomiri-upload-manager
Section: net
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libldm-common0 (= ${binary:Version}),
         libldm-priv-common0 (= ${binary:Version}),
         liblomiri-upload-manager-common0 (= ${binary:Version}),
Description: Lomiri Upload Manager - daemon
 Lomiri Upload Manager performs uploads from a centralized location.
 .
 This package includes the daemon.

Package: qtdeclarative5-lomiri-download-manager0.1
Section: net
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libldm-common0 (= ${binary:Version}),
         liblomiri-download-manager-common0 (= ${binary:Version}),
         liblomiri-download-manager-client0 (= ${binary:Version}),
         lomiri-download-manager (= ${binary:Version})
Description: Lomiri Download Manager Plugin
 This package contains a QML Plugin to handle downloads from a pure QML
 application, without the need to write any C++ code.

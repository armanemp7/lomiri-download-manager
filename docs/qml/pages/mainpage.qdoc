/*!
\page index.html overview
\title Lomiri Download Manager API

\contentspage {Lomiri Download Manager API} {Contents}

\chapter Introduction

The application lifecycle model of found in Lomiri Touch does not
ensure that application will be allowed to perform a long running
connection to a server in order to perform a download. The Lomiri
Download Manager is a system daemon that leverages the long connections
required to perform downloads from the click applications and provides
a set of APIs for the application to interact with the downloads.


The download manager is a centralized daemon and therefore must ensure
that only those applications

\section1 Security

The download manager is a centralized daemon that can be used by all
those applications that are allows to use the networking apparmor
profile, due to this fact the downloading daemon ensures that the
interaction with a download can only be performed either by the
application that originally created the download or by an application
that is not confined and that can therefore interact with all the
daemons of the application.

Once a download has been created the download manager will ensure that
the data being downloaded is stored in a location that can only be
accessed by the creating application to ensure that the app confinement
is kept.

\section1 Components
Available through:
\code
    import Lomiri.DownloadManager 1.2
\endcode

\list
    \li \l {DownloadManager}
    \li \l {SingleDownload}
    \li \l {Metadata}
\endlist

\section1 Example usage - Downloading a file

\qml
    import QtQuick 2.0
    import Lomiri.Components 1.2
    import Lomiri.DownloadManager 1.2

    Rectangle {
        width: units.gu(100)
        height: units.gu(20)

        TextField {
            id: text
            placeholderText: "File URL to download..."
            height: 50
            anchors {
                left: parent.left
                right: button.left
                rightMargin: units.gu(2)
            }
        }

        Button {
            id: button
            text: "Download"
            height: 50
            anchors.right: parent.right

            onClicked: {
                single.download(text.text);
            }
        }

        ProgressBar {
            minimumValue: 0
            maximumValue: 100
            value: single.progress
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            SingleDownload {
                id: single
            }
        }
    }
\endqml

\part Reporting Bugs
If you find any problems with the module or this documentation,
please file a bug in the Lomiri Download Manager \l {https://bugs.launchpad.net/lomiri-download-manager} {Launchpad page}

*/
